<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@home');

Route::get('/register', 'AuthController@register');

Route::get('/selamatdatang', 'AuthController@selamatdatang');

Route::post('/selamatdatang', 'AuthController@submit');

Route::get('/master', function(){
    return view('adminlte.master');
});

Route::get('/masteradmin', function(){
    return view('masteradmin.master');
});

Route::get('/table', function(){
    return view('masteradmin.table');
});

Route::get('/data-tables', function(){
    return view('masteradmin.tables-list');
});