<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function selamatdatang(){
        return view('selamatdatang');
    }

    public function submit(Request $request){
        $nama1 = $request["nama1"];
        $nama2 = $request["nama2"];
        return view('selamatdatang', compact('nama1', 'nama2'));
    }

}
